# ARAT - Automatic Rip And Transcode

A tool for a non-interactive CD ripping machine. Output files are automatically
cleaned up (deleted) after 14 days.

## Requirements

  * A Linux machine running systemd with at least one CD drive.
  * abcde
  * lame
  * eyed3
  * icedax

## Setup

  - Mount your destination storage to /mnt/arat/
  - Run `make install`
  - Enable the systemd service for each CD drive you wish to use:
    - For example, to use /dev/sr0: `systemctl enable arat@sr0`

## Architecture

Instantiatied systemd services that depend on .device units for the CD drive
start the `rip-cd.sh` process when a CD is detected. CD will be ejected at
the completion of the ripping process. Output is saved to `/mnt/arat/` which
can/should be mounted to your storage location of choice (eg, a network share).

Output is recorded to the journal as well as the console, but a screen is not
required for operation - arat is perfectly happy to operate headless. Cleanup
of otuput files is handled by systemd-tmpfiles `/etc/tmpfiles.d/arat.conf`
