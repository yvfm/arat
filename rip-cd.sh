#!/bin/bash

ABCDE_CONF='%%PREFIX%%/abcde.conf'

set -eu

cd_device="$1"

echo "$0 started for device $cd_device"
abcde -N \
  -c "$ABCDE_CONF" \
  -d "$cd_device" \
  -w "Ripped by ARAT on $(date +%Y-%m-%d) $(uname -n)"
echo "$0 complete"
