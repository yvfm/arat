INSTALL = /usr/bin/install -c

prefix = /opt/arat

all:


install:
	$(INSTALL) -m0755 -d $(DESTDIR)$(prefix)
	$(INSTALL) -m0444 abcde.conf $(DESTDIR)$(prefix)/abcde.conf
	$(INSTALL) -m0555 rip-cd.sh $(DESTDIR)$(prefix)/rip-cd.sh
	$(INSTALL) -m0444 arat.service $(DESTDIR)/etc/systemd/system/arat@.service
	$(INSTALL) -m0444 tmpfiles.conf $(DESTDIR)/etc/tmpfiles.d/arat.conf
	sed -e 's|%%PREFIX%%|$(prefix)|g' \
		-i $(DESTDIR)/etc/systemd/system/arat@.service \
		-i $(DESTDIR)$(prefix)/abcde.conf \
		-i $(DESTDIR)$(prefix)/rip-cd.sh
